sap.ui.jsfragment("fragments.HLKFHeader", {
	
	createContent: function (oController) {
        var prefix = oController.prefix;
	
        var orangeHLKFImage = new sap.m.Image(prefix+"orangeHLKFImage",{
            src:"images/hlkf_logo_orange.png",
            height : "45px"
        });
        
        var whiteHLKFImage = new sap.m.Image(prefix+"whiteHLKFImage",{
            src:"images/hlkf_logo_white.png",
            height : "30px"
        });
        
        var title = new sap.m.Title(prefix+"HLKFHeaderTitle",{
        	text : "Hapag-Lloyd Bestellsystem"
        });
        
        function prefixCheckerForTitle(){
        	switch(prefix) {
            case  "RequisitionStart_":
            	title.setText("Kopfdaten");
                break;
            case  "RequisitionDetails_":
            	title.setText("Details");
                break;
            case  "RequisitionChecking_":
            	title.setText("Überprüfung");
                break;   
            default:
            	title.setText("Hapag-Lloyd Bestellsystem");
        	}
        };
        prefixCheckerForTitle();

        
        var logoutButton = new sap.m.Button({
        	text : "Abmelden"
        })
        
        var homeButton = sap.ui.jsfragment("fragments.HLKFHomeButton", oController);
        
		return new sap.m.Bar({
	        contentLeft : [whiteHLKFImage],
	        contentMiddle : [title],
	        contentRight : [logoutButton, homeButton]
		});
	}	
});