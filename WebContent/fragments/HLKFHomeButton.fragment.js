sap.ui.jsfragment("fragments.HLKFHomeButton", {
	
	createContent: function (oController) {
        var prefix = oController.prefix;
	
        var homeButton = new sap.m.Button(prefix+"homeButton", {
        	//text : "Home",
        	icon : "sap-icon://home",
        	press : function(){
        	        var hashChanger = new sap.ui.core.routing.HashChanger();
        	        hashChanger.setHash("");
        	}
        	
        });      
        
		return homeButton;
	}	
});