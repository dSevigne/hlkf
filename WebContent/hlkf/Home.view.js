sap.ui.jsview("hlkf.Home", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf hlkf.Home
	*/ 
	getControllerName : function() {
		return "hlkf.Home";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf hlkf.Home
	*/ 
	createContent : function(oController) {
 		
		
	    var prefix = oController.prefix;
        var newRequisitionTile = new sap.m.StandardTile({
            id : prefix+"newRequisitionTile",
            title : "Neue Bestellung",
            icon : "sap-icon://add",
            info : "Bestellsystem",
            press : oController.navigateToRequisition
        });
        
        var myRequisitionTile = new sap.m.StandardTile({
            id : prefix+"myRequisitionTile",
            title : "Meine Bestellungen",
            icon : "sap-icon://my-sales-order",
            info : "Bestellsystem",
            //press : oController.navigateToRequisition
        });
        
        var searchRequisitionTile = new sap.m.StandardTile({
            id : prefix+"searchRequisitionTile",
            title : "Bestellung suchen",
            icon : "sap-icon://search",
            info : "Bestellsystem",
            //press : oController.navigateToRequisition
        });
        
        var settingsTile = new sap.m.StandardTile({
            id : prefix+"settingsTile",
            title : "Einstellungen",
            icon : "sap-icon://action-settings",
            info : "Bestellsystem",
            press : oController.navigateToRequisition
        });
	    
        var tileContainer = new sap.m.TileContainer({
            id : prefix+"oTile_container",
            tiles : [newRequisitionTile, myRequisitionTile, searchRequisitionTile, settingsTile]
        });
	        
	    var homePage =  new sap.m.Page({
			customHeader : [sap.ui.jsfragment("fragments.HLKFHeader", oController)],
			content : [tileContainer],
			footer : new sap.m.Bar({
				contentLeft : [],
				contentMiddle : [],
				contentRight : []
			})
		});
	return homePage;
	}

});