sap.ui.jsview("hlkf.requisition.RequisitionDetails", {
	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf hlkf.requisition.RequisitionDetails
	*/ 
	getControllerName : function() {
		return "hlkf.requisition.RequisitionDetails";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf hlkf.requisition.RequisitionDetails
	*/ 
	createContent : function(oController) {
		
		var prefix = oController.prefix;
 		
		folderRadioButton = new sap.m.RadioButton(prefix+"folderRadioButton",{
        	text: "Folder"
        });
		
		flyerRadioButton = new sap.m.RadioButton(prefix+"flyerRadioButton",{
        	text: "Flyer"
        });
		
		catalogRadioButton = new sap.m.RadioButton(prefix+"catalogRadioButton",{
        	text: "Katalog"
        });
		
		businessPrintRadioButton = new sap.m.RadioButton(prefix+"businessRadioButton",{
        	text: "Geschäftsdrucksache"
        });
		
		promoRadioButton = new sap.m.RadioButton(prefix+"promoRadioButton",{
        	text: "Verkaufsförderung (z.B. Vertrieb)"
        });
		
		merchRadioButton = new sap.m.RadioButton(prefix+"merchRadioButton",{
        	text: "Merchandising (z.B. Produkt)"
        });

		miscRadioButton = new sap.m.RadioButton(prefix+"miscRadioButton",{
        	text: "Sonstiges"
        });
		
		miscInput = new sap.m.Input(prefix+"miscInput",{
			visible : true,
			enabled : false,
			placeholder : "Sonstiges",
			width : "265px",
		});
		
		objectRadioButtonGroup = sap.m.RadioButtonGroup(prefix+"objectRadioButtonGroup", {
			buttons : [folderRadioButton, flyerRadioButton, catalogRadioButton, businessPrintRadioButton,
			           promoRadioButton, merchRadioButton, miscRadioButton],	
			select : function(){
				oController.miscInputVisible()
			}
		
		});
		
		formatOpenSelect = new sap.m.Select({
			id : prefix+"formatOpenSelect", 
			value : undefined, // string
			//width : "265px",
			items : [new sap.ui.core.Item({
				text : "DIN A4 (210x297mm)", 
				key : "0",
				enabled : true
				}),
				new sap.ui.core.Item({
					text : "DIN A5 (148x210mm)", 
					key : "1",
					enabled : true
				}),
				new sap.ui.core.Item({
					text : "DIN A6 (105 x 148mm)", 
					key : "2",
					enabled : true
				})
			],
			selectedKey: "0"
		});
		
		formatClosedSelect = new sap.m.Select({
			id : prefix+"formatClosedSelect", 
			value : undefined, // string
			//width : "265px",
			items : [new sap.ui.core.Item({
				text : "DIN A4 (210x297mm)", 
				key : "0",
				enabled : true
				}),
				new sap.ui.core.Item({
					text : "DIN A5 (148x210mm)", 
					key : "1",
					enabled : true
				}),
				new sap.ui.core.Item({
					text : "DIN A6 (105 x 148mm)", 
					key : "2",
					enabled : true
				})
			],
			selectedKey: "0"
		});		
		
		printDefaultSelect = new sap.m.Select({
			id : prefix+"printDefaultSelect", 
			value : undefined, // string
			//width : "265px",
			items : [new sap.ui.core.Item({
				text : "beidseitig vierfarbig (4/4)", 
				key : "0",
				enabled : true
				}),
				new sap.ui.core.Item({
					text : "2nd print", 
					key : "1",
					enabled : false
				}),
				new sap.ui.core.Item({
					text : "next way to print", 
					key : "2",
					enabled : false
				})
			],
			selectedKey: "0"
		});	
		
		paperWeightSelect = new sap.m.Select({
			id : prefix+"paperWeightSelect", 
			value : undefined, // string
			//width : "265px",
			items : [new sap.ui.core.Item({
				text : "90g/m² Bilderdruck", 
				key : "0",
				enabled : true
				})
			],
			selectedKey: "0"
		});	
		
		paperContentSelect = new sap.m.Select({
			id : prefix+"paperContentSelect", 
			value : undefined, // string
			//width : "265px",
			items : [new sap.ui.core.Item({
				text : "90g/m² Bilderdruck", 
				key : "0",
				enabled : true
				})
			],
			selectedKey: "0"
		});	
		
		paperCoverSelect = new sap.m.Select({
			id : prefix+"paperCoverSelect", 
			value : undefined, // string
			//width : "265px",
			items : [new sap.ui.core.Item({
				text : "90g/m² Bilderdruck", 
				key : "0",
				enabled : true
				})
			],
			selectedKey: "0"
		});	
		
		otherFormatInput = new sap.m.Input(prefix+"otherFormatInput",{
			value : "",
			placeholder : "z.B. 300x400 mm",
		});
		
		circulationTotalInput = new sap.m.Input(prefix + "circulationTotalInput", {
			placeholder : "Stückzahl",
			liveChange: function(){
		        if(this.getValue() === "")
		            this.setValueState(sap.ui.core.ValueState.Error);
		        else
		            this.setValueState(sap.ui.core.ValueState.Success);
		    }
		});
		
		circulationAlternativeInput = new sap.m.Input(prefix + "circulationAlternativeInput", {
			placeholder : "Stückzahl",
			liveChange: function(){
		        if(this.getValue() === "")
		            this.setValueState(sap.ui.core.ValueState.Error);
		        else
		            this.setValueState(sap.ui.core.ValueState.Success);
		    }
		});
		
	//--- Layouts and Forms (Blöcke)
		
		var requisitionDetailsSimpleForm = new sap.ui.layout.form.SimpleForm(prefix+"requisitionStartSimpleForm", {
					maxContainerCols: 2,
					editable: true,
					content:[
							new sap.ui.core.Title({text:"Objekt"}),
							new sap.m.Label({text:"Produkt wählen"}),
							objectRadioButtonGroup,
							new sap.m.Label({text:""}),
							miscInput,
							new sap.ui.core.Title({text:"Format"}),
							new sap.m.Label({text:"Offen"}),
							formatOpenSelect,
							new sap.m.Label({text:"Geschlossen"}),
							formatClosedSelect,
							new sap.m.Label({text:"Anderes"}),
							otherFormatInput,
							new sap.ui.core.Title({text:"Auflage"}),
							new sap.m.Label({text:"gesamt"}),
							circulationTotalInput,
							new sap.m.Label({text:"alternativ"}),
							circulationAlternativeInput,
							new sap.m.Label({text:"Anderes"}),
							new sap.m.Input({placeholder : "pro Version"}),
							new sap.ui.core.Title({text:"Umfang"}),
							new sap.m.Label({text:"gesamt"}),
							new sap.m.Input({placeholder : "Seitenanzahl"}),
							new sap.m.Label({text:"Umschlag"}),
							new sap.m.Input({placeholder : "Seitenanzahl"}),
							new sap.m.Label({text:"Inhalt"}),
							new sap.m.Input({placeholder : "Seitenanzahl"}),
							new sap.m.Label({text:"Sonstiges"}),
							new sap.m.Input({placeholder : "z.B. Beileger"}),
							new sap.ui.core.Title({text:"Druck"}),
							new sap.m.Label({text:"Standard"}),
							printDefaultSelect,
							new sap.m.Label({text:"Sonstiges"}),
							new sap.m.Input(),
							new sap.ui.core.Title({text:"Papier"}),
							new sap.m.Label({text:"Gewicht"}),
							paperWeightSelect,
							new sap.m.Label({text:"Inhalt"}),
							paperContentSelect,
							new sap.m.Label({text:"Umschlag"}),
							paperCoverSelect,
							new sap.ui.core.Title({text:"Veredelung"}),
							new sap.ui.core.Title({text:"Verarbeitung"}),
							new sap.ui.core.Title({text:"Konfektionierung"}),
							new sap.ui.core.Title({text:"Belegexemplare"})
					]
		});
	
		
		var continueButton = new sap.m.Button(prefix+"continueButton", {
	      	  text : "Weiter",
	      	  press : function () {oController.validateTextFieldValues()},
			});
	
		var requisitionDetailsPage = new sap.m.Page({
			customHeader : [sap.ui.jsfragment("fragments.HLKFHeader", oController)],
			content : [requisitionDetailsSimpleForm],
			footer : new sap.m.Bar({
                contentLeft : [],
                contentMiddle : [],
                contentRight : [continueButton]
			}),
		});
		
		return requisitionDetailsPage;
	}

});