sap.ui.jsview("hlkf.requisition.RequisitionStart", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf hlkf.requisition.RequisitionStart
	*/ 
	getControllerName : function() {
		return "hlkf.requisition.RequisitionStart";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf hlkf.requisition.RequisitionStart
	*/ 
	createContent : function(oController) {
 		var prefix = oController.prefix;
		
 		
 		
 		//							new sap.m.DatePicker({yyyymmdd:"19990909"}),	
 		
		var requisitionStartSimpleForm = new sap.ui.layout.form.SimpleForm(prefix+"requisitionStartSimpleForm", {
			maxContainerCols: 2,
			editable: true,
			content:[
					new sap.ui.core.Title({text:"An"}),
					new sap.ui.core.Title({text:"Von Abteilung"}),
					new sap.ui.core.Title({text:"Stichwort"}),
					new sap.ui.core.Title({text:"Artikel-Nr."}),
					new sap.ui.core.Title({text:"Termin"}),
					new sap.ui.core.Title({text:"Adresse"}),
					new sap.ui.core.Title({text:"Memo"})
			]
		});
				
 		var continueButton = new sap.m.Button(prefix+"continueButton", {
      	  text : "Weiter",
      	  press : oController.continueButtonPressed
		});
		
		var requisitionStartPage = new sap.m.Page({
			customHeader : [sap.ui.jsfragment("fragments.HLKFHeader", oController)],
			content : [requisitionStartSimpleForm],
			footer : new sap.m.Bar({
                contentLeft : [],
                contentMiddle : [],
                contentRight : [continueButton]
			}),
		});
		
		return requisitionStartPage;
	}

});