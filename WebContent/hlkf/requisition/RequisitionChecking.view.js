sap.ui.jsview("hlkf.requisition.RequisitionChecking", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf hlkf.requisition.RequisitionChecking
	*/ 
	getControllerName : function() {
		return "hlkf.requisition.RequisitionChecking";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf hlkf.requisition.RequisitionChecking
	*/ 
	createContent : function(oController) {
		var prefix = oController.prefix;
		
		
		var someRDText = new sap.m.Label(prefix+"someRDText",{
			text : "{requisitionDetailsModel>/objectRadioButtonGroupValue}"
		});
		
		var someInputBox = new sap.m.Input(prefix+"someInputBox", {
			value : "{requisitionDetailsModel>/objectRadioButtonGroupValue}"
		});
		
		
 		var checkingPage = new sap.m.Page(prefix+"checkingPage",{
			title: "Abschließende Überprüfung",
			customHeader : [sap.ui.jsfragment("fragments.HLKFHeader", oController)],
			content: [someRDText, someInputBox],
			footer : new sap.m.Bar({
	            contentLeft : [],
	            contentMiddle : [],
	            contentRight : [new sap.m.Text({text : "Senden"})]
			})
		});
	
	return checkingPage;
	}

});