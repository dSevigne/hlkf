Router = [

                    {
                        pattern: ["", "home", "InitialView"],
                        name: "Home",
                        view: "Home",
                        viewType: sap.ui.core.mvc.ViewType.JS,
                        targetControl: "rootApp",
                        //targetAggregation: "page",
                        clearTarget: true,
                        callback: function(){
                            app.to("homePage");
                        }
                    },
                    {
                        pattern: ["RequisitionStart"],
                        name: "RequisitionStart",
                        view: "RequisitionStart",
                        viewType: sap.ui.core.mvc.ViewType.JS,
                        targetControl: "rootApp",
                        //targetAggregation: "page",
                        clearTarget: true,
                        callback: function(){
                            app.to("requisitionStartPage");
                        }
                    },
                    {
                        pattern: ["RequisitionDetails"],
                        name: "RequisitionDetails",
                        view: "RequisitionDetails",
                        viewType: sap.ui.core.mvc.ViewType.JS,
                        targetControl: "rootApp",
                        //targetAggregation: "page",
                        clearTarget: true,
                        callback: function(){
                            app.to("requisitionDetailsPage");
                        }
                    },
                    {
                        pattern: ["RequisitionChecking"],
                        name: "RequisitionChecking",
                        view: "RequisitionChecking",
                        viewType: sap.ui.core.mvc.ViewType.JS,
                        targetControl: "rootApp",
                        //targetAggregation: "page",
                        clearTarget: true,
                        callback: function(){
                            app.to("requisitionCheckingPage");
                        }
                    }
];